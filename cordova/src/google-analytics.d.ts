export declare class GoogleAnalytics {
    static setTrackingId(trackingId: any): any;
    static sendAppView(screenName: string): any;
    static sendEvent(category: string, action: string, label?: string, value?: number): any;
    static sendTiming(category: string, variable: string, label: string, time: number): any;
    static sendException(description: string, fatal?: boolean): any;
}
