/**
 * Created by jorge on 10/05/2017.
 */

import {GoogleAnalytics} from "./src/google-analytics";

export {GoogleAnalytics} from "./src/google-analytics";