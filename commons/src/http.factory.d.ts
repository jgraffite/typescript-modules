export declare enum METHOD {
    GET = 1,
    POST = 2,
    PUT = 3,
    DELETE = 4,
}
export declare class HttpFactory {
    private requestHeaders;
    timeout: number;
    httpModule: any;
    setHeader(key: any, value: any): void;
    private toUrlParams(obj);
    private treatResponseContent(response, responseContent, responseHeaders);
    private formatRequestBody(requestParams);
    makeHttpRequest(url: string, method: number, params: any, extraHeaders: any, onSuccess: (data: any, response?) => void, onError: (error: any, response?) => void): void;
    makeHttpRequestWithHttp(url: string, method: number, params: any, extraHeaders: any, onSuccess: (data: any, response?) => void, onError: (error: any, response?) => void): any;
    makeHttpRequestWithFetch(url: string, method: number, params: any, extraHeaders: any, onSuccess: (data: any, response?) => void, onError: (error: any, response?) => void): Promise<void>;
    makeHttpRequestWithXmlRequest(url: string, method: number, params: any, extraHeaders: any, onSuccess: (data: any, response?) => void, onError: (error: any, response?) => void): void;
    handleErrors(response: any): any;
}
export declare class HttpRequest {
    private userOnSuccess;
    private userOnError;
    private userOnFinally;
    private httpFactory;
    private headers;
    responseData: any;
    response: any;
    private beforeResult;
    constructor();
    static addBeforeRequests(fn: (url: string, method: METHOD, params: any, extraHeaders: any) => {
        url: string;
        method: METHOD;
        params: any;
        extraHeaders: any;
    }): void;
    static thenOnSuccess(fn: (data: any, response: any, requestOptions: any) => any): void;
    static thenOnError(fn: (data: any, response: any, requestOptions: any) => any): void;
    static thenOnFinally(fn: (data: any, response: any, requestOptions: any) => any): void;
    setDefaultHeader(key: any, value: any): void;
    onSuccess(fn: (data: any) => void): this;
    onError(fn: (data: any, response: any) => void): this;
    thenOnSuccess(fn: (data: any, response: any) => void): this;
    thenOnError(fn: (data: any, response: any) => void): this;
    thenOnFinally(fn: (data: any) => void): this;
    runFns(requestOptions: any, fnList: Array<(data: any, response: any, requestOptions) => any>): void;
    runFnsBefore(resultBefore: {
        url: string;
        method: METHOD;
        params: any;
        extraHeaders: any;
    }): {
        url: string;
        method: METHOD;
        params: any;
        extraHeaders: any;
    };
    callbackOnSuccess(data: any, response: any, requestOptions: any): void;
    callbackOnError(data: any, response: any, requestOptions: any): void;
    setTimeout(timeout: number): void;
    setHeader(key: string, value: any): void;
    get(url: string, urlQuery?: any, headers?: any): this;
    post(url: string, params?: any, headers?: any): this;
    put(url: string, urlQuery?: any, headers?: any): this;
    delete(url: string, urlQuery?: any, headers?: any): this;
}
