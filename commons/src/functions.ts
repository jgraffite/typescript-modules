/**
 * Created by jorge on 25/04/2017
 * version: 1
 */
export function handleError(e: any) {
    console.error(
        "\n"+
        '=============================='+"\n"+
        e.stack+
        "\n"+
        '============================='+"\n"
    );
}

export function consoleAsJson(...args: any[]) {
    args.forEach(something => console.log(JSON.stringify(something, null, "\t")) );
}