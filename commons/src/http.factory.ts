import {consoleAsJson, handleError} from "./functions";

declare let http: any; //for http module of NativeScript
//declare let fetchModule: any; //for fetch module of NativeScript
//declare let Promise: any;
declare let Object: {
    assign: Function;
    keys: Function;
};

export enum METHOD {
    GET = 1,
    POST = 2,
    PUT = 3,
    DELETE = 4
}

export class HttpFactory {

    private requestHeaders: any = {
        'Content-Type': 'application/json;charset=UTF-8'
    };
    public timeout: number = 30000;

    public httpModule: any;


    public setHeader(key, value) {
        this.requestHeaders[key] = value;
    }


    private toUrlParams(obj: {}): string {
        let pairs = [];
        for (let prop in obj) {
            if (!obj.hasOwnProperty(prop)) {
                continue;
            }
            pairs.push(prop + '=' + obj[prop]);
        }
        return pairs.join('&');
    }


    private treatResponseContent(response, responseContent, responseHeaders) {
        if (this.requestHeaders['Content-Type'].indexOf('/json') > -1 || responseHeaders['Content-Type'].indexOf('/json') > -1) {
            try {
                if (response.toJSON) return response.toJSON();
                if (response.json) return response.json().then(data => { return data; });
                if (response.content) if (response.content.toJSON) return response.content.toJSON();
            } catch (e) {

            }

            try {
                return JSON.parse(responseContent);
            } catch (e) {
                return responseContent;
            }
        }
        else return responseContent;
    }

    private formatRequestBody(requestParams) {
        if ( this.requestHeaders['Content-Type'].indexOf('/json') > -1 ) return JSON.stringify(requestParams);
        else return requestParams;
    }


    public makeHttpRequest(url: string, method: number, params: any = {}, extraHeaders: any = {}, onSuccess: (data: any, response?) => void, onError: (error: any, response?) => void) {
        return this.makeHttpRequestWithXmlRequest(url, method, params, extraHeaders, onSuccess, onError);
    }


    public makeHttpRequestWithHttp(url: string, method: number, params: any = {}, extraHeaders: any = {}, onSuccess: (data: any, response?) => void, onError: (error: any, response?) => void) {

        let totalTime = new Date().getTime();

        let requestOptions: any = {
            headers: {}
        };

        /* -----------------------------
            Set request headers
           -----------------------------
         */
        Object.assign(this.requestHeaders, extraHeaders);
        requestOptions.headers = this.requestHeaders;
        /* -----------------------------
            // Set request headers
           -----------------------------
         */

        switch (method) {
            case 1:
            default:

                url += '?'+this.toUrlParams(params);
                requestOptions.method = 'GET';
                break;

            case 2:
                requestOptions.method = 'POST';
                requestOptions.content = this.formatRequestBody(params);
                break;

            case 3:
                url += '?'+this.toUrlParams(params);
                requestOptions.method = 'PUT';
                break;

            case 4:
                url += '?'+this.toUrlParams(params);
                requestOptions.method = 'DELETE';
                break;
        }

        requestOptions.url = url;
        requestOptions.timeout = this.timeout;


        consoleAsJson('Vai requisitar----------');
        consoleAsJson('requestOptions', requestOptions);
        let request = this.httpModule.request(requestOptions)
            .then((response: any) => {
                totalTime = new Date().getTime() - totalTime;
                try {
                    if (response.statusCode == 200) {
                        let content = this.treatResponseContent(response, response.content, response.headers);
                        consoleAsJson('response for '+url, content);
                        onSuccess(content, response);
                    }
                    else {
                        onError('', response);
                        try {
                            consoleAsJson('error 1 for '+url);
                        } catch (e) {
                            consoleAsJson('try catch error 1 for '+url);
                            handleError(e);
                        }
                    }
                } catch (e) {
                    onError('', response);
                    try {
                        consoleAsJson('error 2 for '+url);
                        handleError(e);
                    } catch (e2) {
                        consoleAsJson('try catch error 2 for '+url);
                        handleError(e2);
                    }
                }
            }, (response: any) => {
                onError(this.treatResponseContent(response, response.content, response.headers), response);
            }).catch((reason) => {
                onError(this.treatResponseContent(reason, reason.content, reason.headers), reason);
            });
            /*.then((result: {response: any, data: any}) => {
                onSuccess(result.data, result.response);
            });*/

        return request;
    }


    public makeHttpRequestWithFetch(url: string, method: number, params: any = {}, extraHeaders: any = {}, onSuccess: (data: any, response?) => void, onError: (error: any, response?) => void) {

        let totalTime = new Date().getTime();
        let requestOptions: any = {
            headers: {}
        };


        /* -----------------------------
            Set request headers
           -----------------------------
         */
        Object.assign(this.requestHeaders, extraHeaders);
        requestOptions.headers = this.requestHeaders;
        /* -----------------------------
            // Set request headers
           -----------------------------
         */

        switch (method) {
            case 1:
            default:

                url += '?'+this.toUrlParams(params);
                requestOptions.method = 'GET';
                break;

            case 2:
                requestOptions.method = 'POST';
                requestOptions.body = this.formatRequestBody(params);
                break;

            case 3:
                url += '?'+this.toUrlParams(params);
                requestOptions.method = 'PUT';
                break;

            case 4:
                url += '?'+this.toUrlParams(params);
                requestOptions.method = 'DELETE';
                break;
        }



        var request = fetch(url, requestOptions)
            .then((response: Response) => {
                totalTime = new Date().getTime() - totalTime;

                try {
                    if (response.status == 200) {
                        //teste
                        if (this.requestHeaders['Content-Type'].indexOf('/json') > -1 || response.headers['Content-Type'].indexOf('/json') > -1)
                            response.json().then(data => {
                                onSuccess(data, response);
                            });
                        else
                            response.text().then(data => {
                                onSuccess(data, response);
                            });
                    }
                    else onError('', response);
                } catch (e) {
                    onError('', response);
                    handleError(e);
                }
            }, (response: any) => {
                /*response.clone().text().then((text) => {
                    onError(this.treatResponseContent(response, text, response.headers), response);
                });*/
                onError('', response);
            }).catch((reason) => {
                onError(this.treatResponseContent(reason, reason.content, reason.headers), reason);
            });
            /*.then((response: any) => {
                onSuccess(response);
            });*/

        return request;
    }

    public makeHttpRequestWithXmlRequest(url: string, method: number, params: any = {}, extraHeaders: any = {}, onSuccess: (data: any, response?) => void, onError: (error: any, response?) => void) {


        let totalTime = new Date().getTime();
        let body;
        params['_nc'] = new Date().getTime();

        let finalMethod;
        switch (method) {
            case 1:
            default:
                finalMethod = 'GET';
                url += '?'+this.toUrlParams(params);
                break;

            case 2:
                finalMethod = 'POST';
                body = this.formatRequestBody(params);
                break;

            case 3:
                finalMethod = 'PUT';
                url += '?'+this.toUrlParams(params);
                break;

            case 4:
                finalMethod = 'DELETE';
                url += '?'+this.toUrlParams(params);
                break;
        }


        let req = new XMLHttpRequest();
        req.open(finalMethod, url);


        /* -----------------------------
            Set request headers
           -----------------------------
         */
        Object.assign(this.requestHeaders, extraHeaders);
        Object.keys(this.requestHeaders).forEach((headerName) => {
            let value = this.requestHeaders[headerName];
            req.setRequestHeader(headerName, value);
        });
        /* -----------------------------
            // Set request headers
           -----------------------------
         */


        req.onload = () => {
            totalTime = new Date().getTime() - totalTime;
            //consoleAsJson('TEMPO TOTAL GASTO', (totalTime / 1000));
            if (req.status == 200) {
                try {
                    onSuccess( this.treatResponseContent(req.response, req.response.content, req.response.headers), req.response );
                } catch (e) {
                    onError(this.treatResponseContent(req.response, req.response.content, req.response.headers), req.response);
                }
            } else {
                onError(this.treatResponseContent(req.response, req.response.content, req.response.headers), req.response);
            }
        };

        req.onerror = () => {
            onError(this.treatResponseContent(req.response, req.response.content, req.response.headers), req.response);
        };

        req.ontimeout = () => {
            onError(this.treatResponseContent(req.response, req.response.content, req.response.headers), req.response);
        };

        if (body != undefined) req.send(body);
        else req.send(null);



        /*return new Promise((resolve, reject) => {
            let req = new XMLHttpRequest();
            req.open(method, url);


            /!* -----------------------------
                Set request headers
               -----------------------------
             *!/
            Object.assign(this.requestHeaders, extraHeaders);
            Object.keys(this.requestHeaders).forEach((headerName) => {
                let value = this.requestHeaders[headerName];
                req.setRequestHeader(headerName, value);
            });
            /!* -----------------------------
                // Set request headers
               -----------------------------
             *!/


            req.onload = () => {
                totalTime = new Date().getTime() - totalTime;
                //consoleAsJson('TEMPO TOTAL GASTO', (totalTime / 1000));
                if (req.status == 200) {
                    try {
                        resolve( req.response, this.treatResponseContent(req.response, req.response.content, req.response.headers) );
                    } catch (e) {
                        reject();
                    }
                } else {
                    reject( req.response, this.treatResponseContent(req.response, req.response.content, req.response.headers) );
                }
            };

            req.onerror = function() {
                reject();
            };

            if (body != undefined) {
                req.send(this.formatRequestBody(body));
            } else {
                req.send(null);
            }
        })
            .then((response: any, responseContent: any) => {
                try {
                    onSuccess(responseContent, response);
                } catch (e) {
                    handleError(e);
                }
            }, (response, error: any) => {
                onError(error, response);
            });*/
    }

    handleErrors(response: any) {
        if (!response.ok) throw Error(response.statusText);
        return response;
    }
}


let DEFAULT_HEADERS = {};
let BEFORE_REQUEST: Array<(url: string, method: METHOD, params: any, extraHeaders: any) => {url: string, method: METHOD, params: any, extraHeaders: any} > = [ (url: string, method: METHOD, params: any, extraHeaders: any) => { return {url: url, method: method, params: params, extraHeaders: extraHeaders} } ];
let AFTER_REQUEST_SUCCESS: Array<(data: any, response: any, requestOptions: any) => any> = [(data: any, response: any) => { return data }];
let AFTER_REQUEST_ERROR: Array<(data: any, response: any, requestOptions: any) => any> = [(data: any, response: any) => { return data }];
let AFTER_REQUEST_FINALLY: Array<(data: any, response: any, requestOptions: any) => any> = [(data: any, response: any) => { return data }];


export class HttpRequest {
    private userOnSuccess: Array<(data: any, response: any, requestOptions: any) => any> = [(data: any, response: any) => { return data }];
    private userOnError: Array<(data: any, response: any, requestOptions: any) => any> = [(data: any, response: any) => { return data }];
    private userOnFinally: Array<(data: any, response: any, requestOptions: any) => any> = [(data: any, response: any) => { return data }];
    private httpFactory: HttpFactory = new HttpFactory();
    private headers: any = {};
    public responseData: any = null;
    public response: any = null;

    static timeout = 3000;

    private beforeResult: {url: string, method: METHOD, params: any, extraHeaders: any};


    constructor() {
        this.httpFactory.timeout = HttpRequest.timeout;
        let keys = Object.keys(DEFAULT_HEADERS);
        keys.forEach(headerName => {
            let headerValue = DEFAULT_HEADERS[headerName];
            this.httpFactory.setHeader(headerName, headerValue);
        })
    }

    static addBeforeRequests(fn: (url: string, method: METHOD, params: any, extraHeaders: any) => {url: string, method: METHOD, params: any, extraHeaders: any}) {
        BEFORE_REQUEST.push(fn);
    }

    static thenOnSuccess(fn: (data: any, response: any, requestOptions: any) => any) {
        AFTER_REQUEST_SUCCESS.push(fn);
    }

    static thenOnError(fn: (data: any, response: any, requestOptions: any) => any) {
        AFTER_REQUEST_ERROR.push(fn);
    }

    static thenOnFinally(fn: (data: any, response: any, requestOptions: any) => any) {
        AFTER_REQUEST_FINALLY.push(fn);
    }

    setDefaultHeader(key, value) {
        DEFAULT_HEADERS[key] = value;
    }

    onSuccess(fn: (data: any) => void) {
        this.userOnSuccess = [fn];
        return this;
    }

    onError(fn: (data: any, response: any) => void) {
        this.userOnError = [fn];
        return this;
    }

    thenOnSuccess(fn: (data: any, response: any, requestOptions: any) => any) {
        this.userOnSuccess.push(fn);
        return this;
    }

    thenOnError(fn: (data: any, response: any, requestOptions: any) => any) {
        this.userOnError.push(fn);
        return this;
    }

    thenOnFinally(fn: (data: any, response: any, requestOptions: any) => any) {
        this.userOnFinally.push(fn);
        return this;
    }


    runFns(requestOptions, fnList: Array<(data: any, response: any, requestOptions) => any>): void {
        fnList.forEach((fn: (data: any, response: any, requestOptions) => any) => {
            this.responseData = fn(this.responseData, this.response, requestOptions);
        })
    }

    runFnsBefore(resultBefore: {url: string, method: METHOD, params: any, extraHeaders: any} ): {url: string, method: METHOD, params: any, extraHeaders: any} {
        //let result = {url: null, method: null, params: null, extraHeaders: null};
        BEFORE_REQUEST.forEach((fn: (url: string, method: METHOD, params: any, extraHeaders: any) => {url: string, method: METHOD, params: any, extraHeaders: any}) => {
            resultBefore = fn(resultBefore.url, resultBefore.method, resultBefore.params, resultBefore.extraHeaders);
        });

        return resultBefore;
    }

    callbackOnSuccess(data, response, requestOptions) {
        this.responseData = data;
        this.response = response;
        this.runFns(requestOptions, AFTER_REQUEST_SUCCESS);
        this.runFns(requestOptions, this.userOnSuccess);

        this.runFns(requestOptions, AFTER_REQUEST_FINALLY);
        this.runFns(requestOptions, this.userOnFinally);
    }

    callbackOnError(data, response, requestOptions) {
        this.responseData = data;
        this.response = response;
        this.runFns(requestOptions, AFTER_REQUEST_ERROR);
        this.runFns(requestOptions, this.userOnError);

        this.runFns(requestOptions, AFTER_REQUEST_FINALLY);
        this.runFns(requestOptions, this.userOnFinally);

    }


    setTimeout(timeout: number) {
        this.httpFactory.timeout = timeout;
    }

    setHeader(key: string, value) {
        this.httpFactory.setHeader(key, value);
    }


    get(url: string, urlQuery: any = {}, headers: any = {}) {
        let result = this.runFnsBefore({url: url, method: METHOD.GET, params: urlQuery, extraHeaders: headers});
        url = result.url;
        urlQuery = result.params;
        let method = result.method;
        headers = result.extraHeaders;

        setTimeout(() => {this.httpFactory.makeHttpRequest(url, method, urlQuery, headers, (responseData, response) => { this.callbackOnSuccess(responseData, response, result); }, (error, response) => { this.callbackOnError(error, response, result); })}, 100);
        return this;
    }

    post(url: string, params: any = {}, headers: any = {}) {
        let result = this.runFnsBefore({url: url, method: METHOD.POST, params: params, extraHeaders: headers});
        url = result.url;
        params = result.params;
        let method = result.method;
        headers = result.extraHeaders;

        setTimeout(() => {this.httpFactory.makeHttpRequest(url, method, params, headers, (responseData, response) => { this.callbackOnSuccess(responseData, response, result); }, (error, response) => { this.callbackOnError(error, response, result); })}, 100);
        return this;
    }

    put(url: string, urlQuery: any = {}, headers: any = {}) {
        let result = this.runFnsBefore({url: url, method: METHOD.PUT, params: urlQuery, extraHeaders: headers});
        url = result.url;
        urlQuery = result.params;
        let method = result.method;
        headers = result.extraHeaders;

        setTimeout(() => {this.httpFactory.makeHttpRequest(url, method, urlQuery, headers, (responseData, response) => { this.callbackOnSuccess(responseData, response, result); }, (error, response) => { this.callbackOnError(error, response, result); })}, 100);
        return this;
    }

    delete(url: string, urlQuery: any = {}, headers: any = {}) {
        let result = this.runFnsBefore({url: url, method: METHOD.DELETE, params: urlQuery, extraHeaders: headers});
        url = result.url;
        urlQuery = result.params;
        let method = result.method;
        headers = result.extraHeaders;

        setTimeout(() => {this.httpFactory.makeHttpRequest(url, method, urlQuery, headers, (responseData, response) => { this.callbackOnSuccess(responseData, response, result); }, (error, response) => { this.callbackOnError(error, response, result); })}, 100);
        return this;
    }
}