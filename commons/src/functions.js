"use strict";
exports.__esModule = true;
/**
 * Created by jorge on 25/04/2017
 * version: 1
 */
function handleError(e) {
    console.error("\n" +
        '==============================' + "\n" +
        e.stack +
        "\n" +
        '=============================' + "\n");
}
exports.handleError = handleError;
function consoleAsJson() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    args.forEach(function (something) { return console.log(JSON.stringify(something, null, "\t")); });
}
exports.consoleAsJson = consoleAsJson;
//# sourceMappingURL=functions.js.map