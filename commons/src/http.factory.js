"use strict";
exports.__esModule = true;
var functions_1 = require("./functions");
var METHOD;
(function (METHOD) {
    METHOD[METHOD["GET"] = 1] = "GET";
    METHOD[METHOD["POST"] = 2] = "POST";
    METHOD[METHOD["PUT"] = 3] = "PUT";
    METHOD[METHOD["DELETE"] = 4] = "DELETE";
})(METHOD = exports.METHOD || (exports.METHOD = {}));
var HttpFactory = /** @class */ (function () {
    function HttpFactory() {
        this.requestHeaders = {
            'Content-Type': 'application/json;charset=UTF-8'
        };
        this.timeout = 30000;
    }
    HttpFactory.prototype.setHeader = function (key, value) {
        this.requestHeaders[key] = value;
    };
    HttpFactory.prototype.toUrlParams = function (obj) {
        var pairs = [];
        for (var prop in obj) {
            if (!obj.hasOwnProperty(prop)) {
                continue;
            }
            pairs.push(prop + '=' + obj[prop]);
        }
        return pairs.join('&');
    };
    HttpFactory.prototype.treatResponseContent = function (response, responseContent, responseHeaders) {
        if (this.requestHeaders['Content-Type'].indexOf('/json') > -1 || responseHeaders['Content-Type'].indexOf('/json') > -1) {
            try {
                if (response.toJSON)
                    return response.toJSON();
                if (response.json)
                    return response.json().then(function (data) { return data; });
                if (response.content)
                    if (response.content.toJSON)
                        return response.content.toJSON();
            }
            catch (e) {
            }
            try {
                return JSON.parse(responseContent);
            }
            catch (e) {
                return responseContent;
            }
        }
        else
            return responseContent;
    };
    HttpFactory.prototype.formatRequestBody = function (requestParams) {
        if (this.requestHeaders['Content-Type'].indexOf('/json') > -1)
            return JSON.stringify(requestParams);
        else
            return requestParams;
    };
    HttpFactory.prototype.makeHttpRequest = function (url, method, params, extraHeaders, onSuccess, onError) {
        if (params === void 0) { params = {}; }
        if (extraHeaders === void 0) { extraHeaders = {}; }
        return this.makeHttpRequestWithXmlRequest(url, method, params, extraHeaders, onSuccess, onError);
    };
    HttpFactory.prototype.makeHttpRequestWithHttp = function (url, method, params, extraHeaders, onSuccess, onError) {
        var _this = this;
        if (params === void 0) { params = {}; }
        if (extraHeaders === void 0) { extraHeaders = {}; }
        var totalTime = new Date().getTime();
        var requestOptions = {
            headers: {}
        };
        /* -----------------------------
            Set request headers
           -----------------------------
         */
        Object.assign(this.requestHeaders, extraHeaders);
        requestOptions.headers = this.requestHeaders;
        /* -----------------------------
            // Set request headers
           -----------------------------
         */
        switch (method) {
            case 1:
            default:
                url += '?' + this.toUrlParams(params);
                requestOptions.method = 'GET';
                break;
            case 2:
                requestOptions.method = 'POST';
                requestOptions.content = this.formatRequestBody(params);
                break;
            case 3:
                url += '?' + this.toUrlParams(params);
                requestOptions.method = 'PUT';
                break;
            case 4:
                url += '?' + this.toUrlParams(params);
                requestOptions.method = 'DELETE';
                break;
        }
        requestOptions.url = url;
        requestOptions.timeout = this.timeout;
        functions_1.consoleAsJson('Vai requisitar----------');
        functions_1.consoleAsJson('requestOptions', requestOptions);
        var request = this.httpModule.request(requestOptions)
            .then(function (response) {
            totalTime = new Date().getTime() - totalTime;
            try {
                if (response.statusCode == 200) {
                    var content = _this.treatResponseContent(response, response.content, response.headers);
                    functions_1.consoleAsJson('response for ' + url, content);
                    onSuccess(content, response);
                }
                else {
                    onError('', response);
                    try {
                        functions_1.consoleAsJson('error 1 for ' + url);
                    }
                    catch (e) {
                        functions_1.consoleAsJson('try catch error 1 for ' + url);
                        functions_1.handleError(e);
                    }
                }
            }
            catch (e) {
                onError('', response);
                try {
                    functions_1.consoleAsJson('error 2 for ' + url);
                    functions_1.handleError(e);
                }
                catch (e2) {
                    functions_1.consoleAsJson('try catch error 2 for ' + url);
                    functions_1.handleError(e2);
                }
            }
        }, function (response) {
            onError(_this.treatResponseContent(response, response.content, response.headers), response);
        })["catch"](function (reason) {
            onError(_this.treatResponseContent(reason, reason.content, reason.headers), reason);
        });
        /*.then((result: {response: any, data: any}) => {
            onSuccess(result.data, result.response);
        });*/
        return request;
    };
    HttpFactory.prototype.makeHttpRequestWithFetch = function (url, method, params, extraHeaders, onSuccess, onError) {
        var _this = this;
        if (params === void 0) { params = {}; }
        if (extraHeaders === void 0) { extraHeaders = {}; }
        var totalTime = new Date().getTime();
        var requestOptions = {
            headers: {}
        };
        /* -----------------------------
            Set request headers
           -----------------------------
         */
        Object.assign(this.requestHeaders, extraHeaders);
        requestOptions.headers = this.requestHeaders;
        /* -----------------------------
            // Set request headers
           -----------------------------
         */
        switch (method) {
            case 1:
            default:
                url += '?' + this.toUrlParams(params);
                requestOptions.method = 'GET';
                break;
            case 2:
                requestOptions.method = 'POST';
                requestOptions.body = this.formatRequestBody(params);
                break;
            case 3:
                url += '?' + this.toUrlParams(params);
                requestOptions.method = 'PUT';
                break;
            case 4:
                url += '?' + this.toUrlParams(params);
                requestOptions.method = 'DELETE';
                break;
        }
        var request = fetch(url, requestOptions)
            .then(function (response) {
            totalTime = new Date().getTime() - totalTime;
            try {
                if (response.status == 200) {
                    //teste
                    if (_this.requestHeaders['Content-Type'].indexOf('/json') > -1 || response.headers['Content-Type'].indexOf('/json') > -1)
                        response.json().then(function (data) {
                            onSuccess(data, response);
                        });
                    else
                        response.text().then(function (data) {
                            onSuccess(data, response);
                        });
                }
                else
                    onError('', response);
            }
            catch (e) {
                onError('', response);
                functions_1.handleError(e);
            }
        }, function (response) {
            /*response.clone().text().then((text) => {
                onError(this.treatResponseContent(response, text, response.headers), response);
            });*/
            onError('', response);
        })["catch"](function (reason) {
            onError(_this.treatResponseContent(reason, reason.content, reason.headers), reason);
        });
        /*.then((response: any) => {
            onSuccess(response);
        });*/
        return request;
    };
    HttpFactory.prototype.makeHttpRequestWithXmlRequest = function (url, method, params, extraHeaders, onSuccess, onError) {
        var _this = this;
        if (params === void 0) { params = {}; }
        if (extraHeaders === void 0) { extraHeaders = {}; }
        var totalTime = new Date().getTime();
        var body;
        params['_nc'] = new Date().getTime();
        var finalMethod;
        switch (method) {
            case 1:
            default:
                finalMethod = 'GET';
                url += '?' + this.toUrlParams(params);
                break;
            case 2:
                finalMethod = 'POST';
                body = this.formatRequestBody(params);
                break;
            case 3:
                finalMethod = 'PUT';
                url += '?' + this.toUrlParams(params);
                break;
            case 4:
                finalMethod = 'DELETE';
                url += '?' + this.toUrlParams(params);
                break;
        }
        var req = new XMLHttpRequest();
        req.open(finalMethod, url);
        /* -----------------------------
            Set request headers
           -----------------------------
         */
        Object.assign(this.requestHeaders, extraHeaders);
        Object.keys(this.requestHeaders).forEach(function (headerName) {
            var value = _this.requestHeaders[headerName];
            req.setRequestHeader(headerName, value);
        });
        /* -----------------------------
            // Set request headers
           -----------------------------
         */
        req.onload = function () {
            totalTime = new Date().getTime() - totalTime;
            //consoleAsJson('TEMPO TOTAL GASTO', (totalTime / 1000));
            if (req.status == 200) {
                try {
                    onSuccess(_this.treatResponseContent(req.response, req.response.content, req.response.headers), req.response);
                }
                catch (e) {
                    onError(_this.treatResponseContent(req.response, req.response.content, req.response.headers), req.response);
                }
            }
            else {
                onError(_this.treatResponseContent(req.response, req.response.content, req.response.headers), req.response);
            }
        };
        req.onerror = function () {
            onError(_this.treatResponseContent(req.response, req.response.content, req.response.headers), req.response);
        };
        req.ontimeout = function () {
            onError(_this.treatResponseContent(req.response, req.response.content, req.response.headers), req.response);
        };
        if (body != undefined)
            req.send(body);
        else
            req.send(null);
        /*return new Promise((resolve, reject) => {
            let req = new XMLHttpRequest();
            req.open(method, url);


            /!* -----------------------------
                Set request headers
               -----------------------------
             *!/
            Object.assign(this.requestHeaders, extraHeaders);
            Object.keys(this.requestHeaders).forEach((headerName) => {
                let value = this.requestHeaders[headerName];
                req.setRequestHeader(headerName, value);
            });
            /!* -----------------------------
                // Set request headers
               -----------------------------
             *!/


            req.onload = () => {
                totalTime = new Date().getTime() - totalTime;
                //consoleAsJson('TEMPO TOTAL GASTO', (totalTime / 1000));
                if (req.status == 200) {
                    try {
                        resolve( req.response, this.treatResponseContent(req.response, req.response.content, req.response.headers) );
                    } catch (e) {
                        reject();
                    }
                } else {
                    reject( req.response, this.treatResponseContent(req.response, req.response.content, req.response.headers) );
                }
            };

            req.onerror = function() {
                reject();
            };

            if (body != undefined) {
                req.send(this.formatRequestBody(body));
            } else {
                req.send(null);
            }
        })
            .then((response: any, responseContent: any) => {
                try {
                    onSuccess(responseContent, response);
                } catch (e) {
                    handleError(e);
                }
            }, (response, error: any) => {
                onError(error, response);
            });*/
    };
    HttpFactory.prototype.handleErrors = function (response) {
        if (!response.ok)
            throw Error(response.statusText);
        return response;
    };
    return HttpFactory;
}());
exports.HttpFactory = HttpFactory;
var DEFAULT_HEADERS = {};
var BEFORE_REQUEST = [function (url, method, params, extraHeaders) { return { url: url, method: method, params: params, extraHeaders: extraHeaders }; }];
var AFTER_REQUEST_SUCCESS = [function (data, response) { return data; }];
var AFTER_REQUEST_ERROR = [function (data, response) { return data; }];
var AFTER_REQUEST_FINALLY = [function (data, response) { return data; }];
var HttpRequest = /** @class */ (function () {
    function HttpRequest() {
        var _this = this;
        this.userOnSuccess = [function (data, response) { return data; }];
        this.userOnError = [function (data, response) { return data; }];
        this.userOnFinally = [function (data, response) { return data; }];
        this.httpFactory = new HttpFactory();
        this.headers = {};
        this.responseData = null;
        this.response = null;
        this.httpFactory.timeout = HttpRequest.timeout;
        var keys = Object.keys(DEFAULT_HEADERS);
        keys.forEach(function (headerName) {
            var headerValue = DEFAULT_HEADERS[headerName];
            _this.httpFactory.setHeader(headerName, headerValue);
        });
    }
    HttpRequest.addBeforeRequests = function (fn) {
        BEFORE_REQUEST.push(fn);
    };
    HttpRequest.thenOnSuccess = function (fn) {
        AFTER_REQUEST_SUCCESS.push(fn);
    };
    HttpRequest.thenOnError = function (fn) {
        AFTER_REQUEST_ERROR.push(fn);
    };
    HttpRequest.thenOnFinally = function (fn) {
        AFTER_REQUEST_FINALLY.push(fn);
    };
    HttpRequest.prototype.setDefaultHeader = function (key, value) {
        DEFAULT_HEADERS[key] = value;
    };
    HttpRequest.prototype.onSuccess = function (fn) {
        this.userOnSuccess = [fn];
        return this;
    };
    HttpRequest.prototype.onError = function (fn) {
        this.userOnError = [fn];
        return this;
    };
    HttpRequest.prototype.thenOnSuccess = function (fn) {
        this.userOnSuccess.push(fn);
        return this;
    };
    HttpRequest.prototype.thenOnError = function (fn) {
        this.userOnError.push(fn);
        return this;
    };
    HttpRequest.prototype.thenOnFinally = function (fn) {
        this.userOnFinally.push(fn);
        return this;
    };
    HttpRequest.prototype.runFns = function (requestOptions, fnList) {
        var _this = this;
        fnList.forEach(function (fn) {
            _this.responseData = fn(_this.responseData, _this.response, requestOptions);
        });
    };
    HttpRequest.prototype.runFnsBefore = function (resultBefore) {
        //let result = {url: null, method: null, params: null, extraHeaders: null};
        BEFORE_REQUEST.forEach(function (fn) {
            resultBefore = fn(resultBefore.url, resultBefore.method, resultBefore.params, resultBefore.extraHeaders);
        });
        return resultBefore;
    };
    HttpRequest.prototype.callbackOnSuccess = function (data, response, requestOptions) {
        this.responseData = data;
        this.response = response;
        this.runFns(requestOptions, AFTER_REQUEST_SUCCESS);
        this.runFns(requestOptions, this.userOnSuccess);
        this.runFns(requestOptions, AFTER_REQUEST_FINALLY);
        this.runFns(requestOptions, this.userOnFinally);
    };
    HttpRequest.prototype.callbackOnError = function (data, response, requestOptions) {
        this.responseData = data;
        this.response = response;
        this.runFns(requestOptions, AFTER_REQUEST_ERROR);
        this.runFns(requestOptions, this.userOnError);
        this.runFns(requestOptions, AFTER_REQUEST_FINALLY);
        this.runFns(requestOptions, this.userOnFinally);
    };
    HttpRequest.prototype.setTimeout = function (timeout) {
        this.httpFactory.timeout = timeout;
    };
    HttpRequest.prototype.setHeader = function (key, value) {
        this.httpFactory.setHeader(key, value);
    };
    HttpRequest.prototype.get = function (url, urlQuery, headers) {
        var _this = this;
        if (urlQuery === void 0) { urlQuery = {}; }
        if (headers === void 0) { headers = {}; }
        var result = this.runFnsBefore({ url: url, method: METHOD.GET, params: urlQuery, extraHeaders: headers });
        url = result.url;
        urlQuery = result.params;
        var method = result.method;
        headers = result.extraHeaders;
        setTimeout(function () { _this.httpFactory.makeHttpRequest(url, method, urlQuery, headers, function (responseData, response) { _this.callbackOnSuccess(responseData, response, result); }, function (error, response) { _this.callbackOnError(error, response, result); }); }, 100);
        return this;
    };
    HttpRequest.prototype.post = function (url, params, headers) {
        var _this = this;
        if (params === void 0) { params = {}; }
        if (headers === void 0) { headers = {}; }
        var result = this.runFnsBefore({ url: url, method: METHOD.POST, params: params, extraHeaders: headers });
        url = result.url;
        params = result.params;
        var method = result.method;
        headers = result.extraHeaders;
        setTimeout(function () { _this.httpFactory.makeHttpRequest(url, method, params, headers, function (responseData, response) { _this.callbackOnSuccess(responseData, response, result); }, function (error, response) { _this.callbackOnError(error, response, result); }); }, 100);
        return this;
    };
    HttpRequest.prototype.put = function (url, urlQuery, headers) {
        var _this = this;
        if (urlQuery === void 0) { urlQuery = {}; }
        if (headers === void 0) { headers = {}; }
        var result = this.runFnsBefore({ url: url, method: METHOD.PUT, params: urlQuery, extraHeaders: headers });
        url = result.url;
        urlQuery = result.params;
        var method = result.method;
        headers = result.extraHeaders;
        setTimeout(function () { _this.httpFactory.makeHttpRequest(url, method, urlQuery, headers, function (responseData, response) { _this.callbackOnSuccess(responseData, response, result); }, function (error, response) { _this.callbackOnError(error, response, result); }); }, 100);
        return this;
    };
    HttpRequest.prototype["delete"] = function (url, urlQuery, headers) {
        var _this = this;
        if (urlQuery === void 0) { urlQuery = {}; }
        if (headers === void 0) { headers = {}; }
        var result = this.runFnsBefore({ url: url, method: METHOD.DELETE, params: urlQuery, extraHeaders: headers });
        url = result.url;
        urlQuery = result.params;
        var method = result.method;
        headers = result.extraHeaders;
        setTimeout(function () { _this.httpFactory.makeHttpRequest(url, method, urlQuery, headers, function (responseData, response) { _this.callbackOnSuccess(responseData, response, result); }, function (error, response) { _this.callbackOnError(error, response, result); }); }, 100);
        return this;
    };
    HttpRequest.timeout = 3000;
    return HttpRequest;
}());
exports.HttpRequest = HttpRequest;
//# sourceMappingURL=http.factory.js.map