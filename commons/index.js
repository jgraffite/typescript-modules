"use strict";
/**
 * Created by jorge on 10/05/2017.
 */
exports.__esModule = true;
if (!Object.hasOwnProperty('assign')) {
    Object.defineProperty(Object, 'assign', {
        enumerable: false,
        configurable: true,
        writable: true,
        value: function (target) {
            'use strict';
            if (target === undefined || target === null) {
                throw new TypeError('Cannot convert first argument to object');
            }
            var to = Object(target);
            for (var i = 1; i < arguments.length; i++) {
                var nextSource = arguments[i];
                if (nextSource === undefined || nextSource === null) {
                    continue;
                }
                nextSource = Object(nextSource);
                var keysArray = Object.keys(nextSource);
                for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
                    var nextKey = keysArray[nextIndex];
                    var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                    if (desc !== undefined && desc.enumerable) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
            return to;
        }
    });
}
var functions_1 = require("./src/functions");
exports.handleError = functions_1.handleError;
var functions_2 = require("./src/functions");
exports.consoleAsJson = functions_2.consoleAsJson;
var filter_pipe_1 = require("./src/filter.pipe");
exports.FilterPipe = filter_pipe_1.FilterPipe;
var order_by_pipe_1 = require("./src/order-by.pipe");
exports.OrderByPipe = order_by_pipe_1.OrderByPipe;
var http_factory_1 = require("./src/http.factory");
exports.HttpFactory = http_factory_1.HttpFactory;
var http_factory_2 = require("./src/http.factory");
exports.HttpRequest = http_factory_2.HttpRequest;
//# sourceMappingURL=index.js.map