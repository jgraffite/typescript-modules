"use strict";
/* ---------------------------------------------
    This module is using this cordova plugin: https://github.com/cmackay/google-analytics-plugin
    --------------------------------------------------------------------------------
 */
exports.__esModule = true;
var analytics = navigator.analytics;
var GoogleAnalytics = /** @class */ (function () {
    function GoogleAnalytics() {
    }
    GoogleAnalytics.setTrackingId = function (trackingId) {
        return new Promise(function (resolve, reject) {
            if (analytics != undefined)
                analytics.setTrackingId(trackingId, resolve, reject);
        });
    };
    GoogleAnalytics.sendAppView = function (screenName) {
        return new Promise(function (resolve, reject) {
            if (analytics != undefined)
                analytics.sendAppView(screenName, resolve, reject);
        });
    };
    GoogleAnalytics.sendEvent = function (category, action, label, value) {
        return new Promise(function (resolve, reject) {
            if (analytics != undefined)
                analytics.sendAppView(category, action, label, value, resolve, reject);
        });
    };
    GoogleAnalytics.sendTiming = function (category, variable, label, time) {
        return new Promise(function (resolve, reject) {
            if (analytics != undefined)
                analytics.sendTiming(category, variable, label, time, resolve, reject);
        });
    };
    GoogleAnalytics.sendException = function (description, fatal) {
        return new Promise(function (resolve, reject) {
            if (analytics != undefined)
                analytics.sendException(description, fatal, resolve, reject);
        });
    };
    return GoogleAnalytics;
}());
exports.GoogleAnalytics = GoogleAnalytics;
//# sourceMappingURL=google-analytics.js.map