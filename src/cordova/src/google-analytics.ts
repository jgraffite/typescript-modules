/* ---------------------------------------------
    This module is using this cordova plugin: https://github.com/cmackay/google-analytics-plugin
    --------------------------------------------------------------------------------
 */

declare let navigator: {analytics: any};
let analytics: any = navigator.analytics;
declare let Promise: any;

export class GoogleAnalytics {

    static setTrackingId(trackingId) {
        return new Promise((resolve: () => void, reject: () => void) => {
            if (analytics != undefined) analytics.setTrackingId(trackingId, resolve, reject);
        });
    }

    static sendAppView(screenName: string) {
        return new Promise((resolve: () => void, reject: () => void) => {
            if (analytics != undefined) analytics.sendAppView(screenName, resolve, reject);
        });
    }

    static sendEvent(category: string, action: string, label?: string, value?: number) {
        return new Promise((resolve: () => void, reject: () => void) => {
            if (analytics != undefined) analytics.sendAppView(category, action, label, value, resolve, reject);
        });
    }

    static sendTiming(category: string, variable: string, label: string, time: number) {
        return new Promise((resolve: () => void, reject: () => void) => {
            if (analytics != undefined) analytics.sendTiming(category, variable, label, time, resolve, reject);
        });
    }

    static sendException(description: string, fatal?: boolean) {
        return new Promise((resolve: () => void, reject: () => void) => {
            if (analytics != undefined) analytics.sendException(description, fatal, resolve, reject);
        });
    }
}