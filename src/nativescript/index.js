"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
exports.__esModule = true;
exports.HttpFactory = exports.HttpRequest = exports.TNSGoogleAnalytics = void 0;
/**
 * Created by jorge on 10/05/2017.
 */
var httpModule = require("tns-core-modules/http/http-request");
var commons_1 = require("../commons");
commons_1.HttpFactory.prototype.httpModule = httpModule;
commons_1.HttpFactory.prototype.makeHttpRequest = function (url, method, params, extraHeaders, onSuccess, onError) {
    this.makeHttpRequestWithHttp(url, method, params, extraHeaders, onSuccess, onError);
};
var tns_google_analytics_1 = require("./src/tns-google-analytics");
__createBinding(exports, tns_google_analytics_1, "TNSGoogleAnalytics");
var index_1 = require("../commons/index");
__createBinding(exports, index_1, "HttpRequest");
__createBinding(exports, index_1, "HttpFactory");
