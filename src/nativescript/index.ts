/**
 * Created by jorge on 10/05/2017.
 */
import * as httpModule from "tns-core-modules/http/http-request";
//var http = require("http/http-request");

import {TNSGoogleAnalytics} from "./src/tns-google-analytics";
import {HttpRequest, HttpFactory} from "../commons";
HttpFactory.prototype.httpModule = httpModule;
HttpFactory.prototype.makeHttpRequest = function(url, method, params, extraHeaders, onSuccess, onError) {
    this.makeHttpRequestWithHttp(url, method, params, extraHeaders, onSuccess, onError);
};

export {TNSGoogleAnalytics} from "./src/tns-google-analytics";
export {HttpRequest, HttpFactory} from "../commons/index";
