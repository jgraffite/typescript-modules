"use strict";
exports.__esModule = true;
exports.TNSGoogleAnalytics = void 0;
var googleAnalytics = require("nativescript-google-analytics");
var application = require("tns-core-modules/application");
var TNSGoogleAnalytics = /** @class */ (function () {
    function TNSGoogleAnalytics() {
    }
    TNSGoogleAnalytics.init = function (trackingId) {
        googleAnalytics.initalize({
            trackingId: trackingId,
            dispatchInterval: 5,
            logging: {
                native: true,
                console: false
            }
        });
    };
    TNSGoogleAnalytics.logScreenView = function (screenName) {
        googleAnalytics.logView(screenName);
    };
    TNSGoogleAnalytics.logEvent = function (category, action, label, value) {
        if (value === void 0) { value = 1; }
        googleAnalytics.logEvent({
            category: category,
            action: action,
            label: label,
            value: value
        });
    };
    TNSGoogleAnalytics.startTimer = function (timerKey, category, name) {
        googleAnalytics.startTimer(timerKey, {
            category: category,
            name: name,
            label: (application.ios) ? "iOS" : "Android" //Optional
        });
        return function () {
            this.stopTimer = function () {
                googleAnalytics.stopTimer(timerKey);
            };
        };
    };
    TNSGoogleAnalytics.logTimingEvent = function (category, totalMilisecconds, name, label) {
        googleAnalytics.logTimingEvent({
            category: category,
            value: totalMilisecconds,
            name: name,
            label: label //Optional
        });
    };
    return TNSGoogleAnalytics;
}());
exports.TNSGoogleAnalytics = TNSGoogleAnalytics;
