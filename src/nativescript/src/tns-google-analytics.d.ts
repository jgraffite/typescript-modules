declare module "nativescript-google-analytics" {
}
export declare class TNSGoogleAnalytics {
    static init(trackingId: string): void;
    static logScreenView(screenName: string): void;
    static logEvent(category: string, action: string, label?: string, value?: number): void;
    static startTimer(timerKey: string, category: string, name?: string): () => void;
    static logTimingEvent(category: string, totalMilisecconds: number, name?: string, label?: string): void;
}
