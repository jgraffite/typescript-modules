"use strict";
/**
 * Created by jorge on 10/05/2017.
 */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
exports.__esModule = true;
exports.HttpRequest = exports.HttpFactory = exports.OrderByPipe = exports.FilterPipe = exports.consoleAsJson = exports.handleError = void 0;
if (!Object.hasOwnProperty('assign')) {
    Object.defineProperty(Object, 'assign', {
        enumerable: false,
        configurable: true,
        writable: true,
        value: function (target) {
            'use strict';
            if (target === undefined || target === null) {
                throw new TypeError('Cannot convert first argument to object');
            }
            var to = Object(target);
            for (var i = 1; i < arguments.length; i++) {
                var nextSource = arguments[i];
                if (nextSource === undefined || nextSource === null) {
                    continue;
                }
                nextSource = Object(nextSource);
                var keysArray = Object.keys(nextSource);
                for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
                    var nextKey = keysArray[nextIndex];
                    var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                    if (desc !== undefined && desc.enumerable) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
            return to;
        }
    });
}
var functions_1 = require("./src/functions");
__createBinding(exports, functions_1, "handleError");
var functions_2 = require("./src/functions");
__createBinding(exports, functions_2, "consoleAsJson");
var filter_pipe_1 = require("./src/filter.pipe");
__createBinding(exports, filter_pipe_1, "FilterPipe");
var order_by_pipe_1 = require("./src/order-by.pipe");
__createBinding(exports, order_by_pipe_1, "OrderByPipe");
var http_factory_1 = require("./src/http.factory");
__createBinding(exports, http_factory_1, "HttpFactory");
var http_factory_2 = require("./src/http.factory");
__createBinding(exports, http_factory_2, "HttpRequest");
