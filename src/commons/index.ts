/**
 * Created by jorge on 10/05/2017.
 */

import {handleError} from "./src/functions";
import {consoleAsJson} from "./src/functions";
import {FilterPipe} from "./src/filter.pipe";
import {OrderByPipe} from "./src/order-by.pipe";
import {HttpRequest} from "./src/http.factory";

if (!Object.hasOwnProperty('assign')) {
    Object.defineProperty(Object, 'assign', {
        enumerable: false,
        configurable: true,
        writable: true,
        value: function(target) {
            'use strict';
            if (target === undefined || target === null) {
                throw new TypeError('Cannot convert first argument to object');
            }

            var to = Object(target);
            for (var i = 1; i < arguments.length; i++) {
                var nextSource = arguments[i];
                if (nextSource === undefined || nextSource === null) {
                    continue;
                }
                nextSource = Object(nextSource);

                var keysArray = Object.keys(nextSource);
                for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
                    var nextKey = keysArray[nextIndex];
                    var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                    if (desc !== undefined && desc.enumerable) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
            return to;
        }
    });
}

export {handleError} from "./src/functions";
export {consoleAsJson} from "./src/functions";
export {FilterPipe} from "./src/filter.pipe";
export {OrderByPipe} from "./src/order-by.pipe";
export {HttpFactory} from "./src/http.factory";
export {HttpRequest} from "./src/http.factory";
