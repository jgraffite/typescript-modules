/**
 * Created by jorge on 25/04/2017
 * version: 1
 */
import {consoleAsJson} from "./functions";
declare let Object: {
    assign: Function;
    keys: Function;
};

export class FilterPipe {
    public transform(obj: any, filterFields: Array<Array<any>>): any {

        var returnObj =  obj.filter((current: any) => {

            var comparisonReturn: boolean = true;

            for(var key = 0; key < filterFields.length; key++ ){
                /*filterFields.forEach((currentFilter, key) => {*/

                var currentField = filterFields[key];
                var fieldName = currentField[0];
                var operator = currentField[1];
                var condValue = currentField[2];
                //var currentValue = JSON.parse(JSON.stringify(current));
                var currentValue = Object.assign({}, current);
                var caseInsensitive = fieldName.indexOf('+i+') > -1;
                if (caseInsensitive) fieldName = fieldName.replace('+i+', '');

                var explodedField: Array<String> = fieldName.split('.');
                explodedField.forEach((field: string) => {
                    currentValue = currentValue[field];
                });

                if (caseInsensitive) {
                    if (typeof condValue == 'string') condValue = condValue.toLowerCase();
                    if (typeof currentValue == 'string') currentValue = currentValue.toLowerCase();
                }

                if (operator == '==') {
                    if (currentValue != condValue) {
                        comparisonReturn = false;
                        continue;
                    }
                } else if (operator == '!=') {
                    if (currentValue == condValue) {
                        comparisonReturn = false;
                        continue;
                    }
                } else if (operator == '>') {
                    if (currentValue < condValue) {
                        comparisonReturn = false;
                        continue;
                    }
                } else if (operator == '<') {
                    if (currentValue > condValue) {
                        comparisonReturn = false;
                        continue;
                    }
                } else if (operator == 'contains') {
                    if (currentValue.indexOf(condValue) == -1) {
                        comparisonReturn = false;
                        continue;
                    }
                } else if (operator == 'partOf' || operator == 'indexOf') {
                    if (currentValue.indexOf(condValue) == -1) {
                        comparisonReturn = false;
                        continue;
                    }
                } else if (operator == 'startsWith') {
                    if (currentValue.indexOf(condValue) != 0) {
                        comparisonReturn = false;
                        continue;
                    }
                } else if (operator == 'notContains') {
                    if (currentValue.indexOf(condValue) > -1) {
                        comparisonReturn = false;
                        continue;
                    }
                }
            }

            return comparisonReturn;
        });

        return returnObj;
    }

    public filter(obj: any, filterFields: Array<Array<any>>): Array<any> {
        return this.transform(obj, filterFields);
    }
}