import {consoleAsJson} from "./functions";

declare let Object: {
    assign: Function;
    keys: Function;
};

export class OrderByPipe {

    public orderFields: Array<any>;

    public transform(obj: any, orderFields: Array<any>): any {
        if (typeof orderFields == 'string') orderFields = [orderFields];
        //orderFields.reverse();

        //let that = this;
        let clonedArray = obj.slice();
        clonedArray.sort((a: any, b: any) => {
            return this.orderCoreFunction(a, b, orderFields);
        });

        return clonedArray;
    }


    public orderCoreFunction(a: any, b: any, orderFields: Array<any>) {

        //console.log(a.match.homeTeam.name+' x '+a.match.awayTeam.name, b.match.homeTeam.name+' x '+b.match.awayTeam.name, 'Não precisou ir para o campo dois');
        //return false;

        let orderType;

        let finalResult;
        let finalField;
        for (let key = 0; key < orderFields.length; key++ ){

            let currentField = orderFields[key];
            let arrayCompare: any[];
            if (typeof currentField == 'object') {
                arrayCompare = currentField;
                currentField = currentField[0];
            }
            let copiedA: {} = /*JSON.parse(JSON.stringify(a));*/Object.assign({}, a);
            let copiedB: {} = /*JSON.parse(JSON.stringify(b));*/Object.assign({}, b);

            orderType = 'ASC';
            if (currentField[0] === '-') {
                currentField = currentField.substring(1);
                orderType = 'DESC';
            }
            if (currentField.indexOf('DESC:') > -1) {
                currentField = currentField.replace('DESC:', '');
                orderType = 'DESC';
            } else currentField = currentField.replace('ASC:', '');


            let explodedField: string[] = currentField.split('.');
            explodedField.forEach((field: string) => {
                /*se a propriedade informada for inexistente*/
                if (!copiedA.hasOwnProperty(field) || !copiedB.hasOwnProperty(field)) {
                    copiedA = null;
                    copiedB = null;
                    return false;
                }
                copiedA = copiedA[field];
                copiedB = copiedB[field];
            });

            /*se a propriedade informada for inexistente, joga o objeto para o último lugar */
            if (copiedA === null) return (orderType == 'ASC') ? -1 : 1;
            if (copiedB === null) return (orderType == 'ASC') ? 1 : -1;

            let localResult;
            if (arrayCompare) localResult = this.compareValue(copiedA, copiedB, orderType, arrayCompare[1], arrayCompare[2]);
            else localResult = this.compare(copiedA, copiedB, orderType);

            if (localResult !== 0 || (key+1) == orderFields.length) {
                //console.log(a.match.homeTeam.name+' x '+a.match.awayTeam.name, ' - ', b.match.homeTeam.name+' x '+b.match.awayTeam.name, ' - Não precisou ir para o campo dois', currentField, copiedA, ' - ', copiedB, orderType);
                finalResult = localResult;
                finalField = currentField;
                break;
            }

            //console.log(a.match.homeTeam.name+' x '+a.match.awayTeam.name, ' - ', b.match.homeTeam.name+' x '+b.match.awayTeam.name, ' - Precisou ir para o campo dois', currentField, copiedA, ' - ', copiedB, orderType);
        }

        //console.log('RESULTADO FINAL - ',a.match.homeTeam.name+' x '+a.match.awayTeam.name, ' - ', b.match.homeTeam.name+' x '+b.match.awayTeam.name, ' - ', finalResult, ' - campo: ', finalField, "\n\n");
        return finalResult;
    }


    private compare (a: any, b: any, orderType: string) {
        if (orderType == 'ASC') {
            if (a < b) return -1;
            if (a > b) return 1;
            return 0;
        } else {
            if (a < b) return 1;
            if (a > b) return -1;
            return 0;
        }
    }

    private compareValue (a: any, b: any, orderType: string, operator: string, value: any) {
        let finalResult = 0;
        let returnA, returnB;
        if (orderType == 'ASC') {
            returnA = -1;
            returnB = 1;
        } else {
            returnA = 1;
            returnB = -1;
        }

        if (operator == '==') {
            if (a == value && b != value) finalResult = returnA;
            else if (a != value && b == value) finalResult = returnB;
            else finalResult = 0;

        } else if (operator == '!=') {
            if (a != value && b == value) finalResult = returnA;
            else if (a == value && b != value) finalResult = returnB;
            else finalResult = 0;
        } else if (operator == '>') {
            if (a > value && b <= value) finalResult = returnA;
            else if (a <= value && b > value) finalResult = returnB;
            else finalResult = 0;
        } else if (operator == '<') {
            if (a < value && b >= value) finalResult = returnA;
            else if (a >= value && b < value) finalResult = returnB;
            else finalResult = 0;
        }

        //consoleAsJson('ORDER', orderType, 'operator', operator, 'a', a ,'value', value, 'return', finalResult, '\n\n');
        return finalResult;
    }

    public order(obj: any, orderByfields: any): Array<any> {
        return this.transform(obj, orderByfields);
    }
}