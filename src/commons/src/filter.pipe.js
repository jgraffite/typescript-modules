"use strict";
exports.__esModule = true;
exports.FilterPipe = void 0;
var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (obj, filterFields) {
        var returnObj = obj.filter(function (current) {
            var comparisonReturn = true;
            for (var key = 0; key < filterFields.length; key++) {
                /*filterFields.forEach((currentFilter, key) => {*/
                var currentField = filterFields[key];
                var fieldName = currentField[0];
                var operator = currentField[1];
                var condValue = currentField[2];
                //var currentValue = JSON.parse(JSON.stringify(current));
                var currentValue = Object.assign({}, current);
                var caseInsensitive = fieldName.indexOf('+i+') > -1;
                if (caseInsensitive)
                    fieldName = fieldName.replace('+i+', '');
                var explodedField = fieldName.split('.');
                explodedField.forEach(function (field) {
                    currentValue = currentValue[field];
                });
                if (caseInsensitive) {
                    if (typeof condValue == 'string')
                        condValue = condValue.toLowerCase();
                    if (typeof currentValue == 'string')
                        currentValue = currentValue.toLowerCase();
                }
                if (operator == '==') {
                    if (currentValue != condValue) {
                        comparisonReturn = false;
                        continue;
                    }
                }
                else if (operator == '!=') {
                    if (currentValue == condValue) {
                        comparisonReturn = false;
                        continue;
                    }
                }
                else if (operator == '>') {
                    if (currentValue < condValue) {
                        comparisonReturn = false;
                        continue;
                    }
                }
                else if (operator == '<') {
                    if (currentValue > condValue) {
                        comparisonReturn = false;
                        continue;
                    }
                }
                else if (operator == 'contains') {
                    if (currentValue.indexOf(condValue) == -1) {
                        comparisonReturn = false;
                        continue;
                    }
                }
                else if (operator == 'partOf' || operator == 'indexOf') {
                    if (currentValue.indexOf(condValue) == -1) {
                        comparisonReturn = false;
                        continue;
                    }
                }
                else if (operator == 'startsWith') {
                    if (currentValue.indexOf(condValue) != 0) {
                        comparisonReturn = false;
                        continue;
                    }
                }
                else if (operator == 'notContains') {
                    if (currentValue.indexOf(condValue) > -1) {
                        comparisonReturn = false;
                        continue;
                    }
                }
            }
            return comparisonReturn;
        });
        return returnObj;
    };
    FilterPipe.prototype.filter = function (obj, filterFields) {
        return this.transform(obj, filterFields);
    };
    return FilterPipe;
}());
exports.FilterPipe = FilterPipe;
