"use strict";
exports.__esModule = true;
exports.OrderByPipe = void 0;
var OrderByPipe = /** @class */ (function () {
    function OrderByPipe() {
    }
    OrderByPipe.prototype.transform = function (obj, orderFields) {
        var _this = this;
        if (typeof orderFields == 'string')
            orderFields = [orderFields];
        //orderFields.reverse();
        //let that = this;
        var clonedArray = obj.slice();
        clonedArray.sort(function (a, b) {
            return _this.orderCoreFunction(a, b, orderFields);
        });
        return clonedArray;
    };
    OrderByPipe.prototype.orderCoreFunction = function (a, b, orderFields) {
        //console.log(a.match.homeTeam.name+' x '+a.match.awayTeam.name, b.match.homeTeam.name+' x '+b.match.awayTeam.name, 'Não precisou ir para o campo dois');
        //return false;
        var orderType;
        var finalResult;
        var finalField;
        var _loop_1 = function (key) {
            var currentField = orderFields[key];
            var arrayCompare = void 0;
            if (typeof currentField == 'object') {
                arrayCompare = currentField;
                currentField = currentField[0];
            }
            var copiedA = Object.assign({}, a);
            var copiedB = Object.assign({}, b);
            orderType = 'ASC';
            if (currentField[0] === '-') {
                currentField = currentField.substring(1);
                orderType = 'DESC';
            }
            if (currentField.indexOf('DESC:') > -1) {
                currentField = currentField.replace('DESC:', '');
                orderType = 'DESC';
            }
            else
                currentField = currentField.replace('ASC:', '');
            var explodedField = currentField.split('.');
            explodedField.forEach(function (field) {
                /*se a propriedade informada for inexistente*/
                if (!copiedA.hasOwnProperty(field) || !copiedB.hasOwnProperty(field)) {
                    copiedA = null;
                    copiedB = null;
                    return false;
                }
                copiedA = copiedA[field];
                copiedB = copiedB[field];
            });
            /*se a propriedade informada for inexistente, joga o objeto para o último lugar */
            if (copiedA === null)
                return { value: (orderType == 'ASC') ? -1 : 1 };
            if (copiedB === null)
                return { value: (orderType == 'ASC') ? 1 : -1 };
            var localResult = void 0;
            if (arrayCompare)
                localResult = this_1.compareValue(copiedA, copiedB, orderType, arrayCompare[1], arrayCompare[2]);
            else
                localResult = this_1.compare(copiedA, copiedB, orderType);
            if (localResult !== 0 || (key + 1) == orderFields.length) {
                //console.log(a.match.homeTeam.name+' x '+a.match.awayTeam.name, ' - ', b.match.homeTeam.name+' x '+b.match.awayTeam.name, ' - Não precisou ir para o campo dois', currentField, copiedA, ' - ', copiedB, orderType);
                finalResult = localResult;
                finalField = currentField;
                return "break";
            }
        };
        var this_1 = this;
        for (var key = 0; key < orderFields.length; key++) {
            var state_1 = _loop_1(key);
            if (typeof state_1 === "object")
                return state_1.value;
            if (state_1 === "break")
                break;
        }
        //console.log('RESULTADO FINAL - ',a.match.homeTeam.name+' x '+a.match.awayTeam.name, ' - ', b.match.homeTeam.name+' x '+b.match.awayTeam.name, ' - ', finalResult, ' - campo: ', finalField, "\n\n");
        return finalResult;
    };
    OrderByPipe.prototype.compare = function (a, b, orderType) {
        if (orderType == 'ASC') {
            if (a < b)
                return -1;
            if (a > b)
                return 1;
            return 0;
        }
        else {
            if (a < b)
                return 1;
            if (a > b)
                return -1;
            return 0;
        }
    };
    OrderByPipe.prototype.compareValue = function (a, b, orderType, operator, value) {
        var finalResult = 0;
        var returnA, returnB;
        if (orderType == 'ASC') {
            returnA = -1;
            returnB = 1;
        }
        else {
            returnA = 1;
            returnB = -1;
        }
        if (operator == '==') {
            if (a == value && b != value)
                finalResult = returnA;
            else if (a != value && b == value)
                finalResult = returnB;
            else
                finalResult = 0;
        }
        else if (operator == '!=') {
            if (a != value && b == value)
                finalResult = returnA;
            else if (a == value && b != value)
                finalResult = returnB;
            else
                finalResult = 0;
        }
        else if (operator == '>') {
            if (a > value && b <= value)
                finalResult = returnA;
            else if (a <= value && b > value)
                finalResult = returnB;
            else
                finalResult = 0;
        }
        else if (operator == '<') {
            if (a < value && b >= value)
                finalResult = returnA;
            else if (a >= value && b < value)
                finalResult = returnB;
            else
                finalResult = 0;
        }
        //consoleAsJson('ORDER', orderType, 'operator', operator, 'a', a ,'value', value, 'return', finalResult, '\n\n');
        return finalResult;
    };
    OrderByPipe.prototype.order = function (obj, orderByfields) {
        return this.transform(obj, orderByfields);
    };
    return OrderByPipe;
}());
exports.OrderByPipe = OrderByPipe;
