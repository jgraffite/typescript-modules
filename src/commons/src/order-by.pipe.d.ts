export declare class OrderByPipe {
    orderFields: Array<any>;
    transform(obj: any, orderFields: Array<any>): any;
    orderCoreFunction(a: any, b: any, orderFields: Array<any>): any;
    private compare;
    private compareValue;
    order(obj: any, orderByfields: any): Array<any>;
}
