export declare class FilterPipe {
    transform(obj: any, filterFields: Array<Array<any>>): any;
    filter(obj: any, filterFields: Array<Array<any>>): Array<any>;
}
