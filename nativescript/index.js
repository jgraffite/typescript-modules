"use strict";
exports.__esModule = true;
/**
 * Created by jorge on 10/05/2017.
 */
var httpModule = require("tns-core-modules/http/http-request");
var commons_1 = require("../commons");
commons_1.HttpFactory.prototype.httpModule = httpModule;
commons_1.HttpFactory.prototype.makeHttpRequest = function (url, method, params, extraHeaders, onSuccess, onError) {
    this.makeHttpRequestWithHttp(url, method, params, extraHeaders, onSuccess, onError);
};
var tns_google_analytics_1 = require("./src/tns-google-analytics");
exports.TNSGoogleAnalytics = tns_google_analytics_1.TNSGoogleAnalytics;
var index_1 = require("../commons/index");
exports.HttpRequest = index_1.HttpRequest;
exports.HttpFactory = index_1.HttpFactory;
//# sourceMappingURL=index.js.map