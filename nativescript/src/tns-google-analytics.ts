/*
 * Based on Nativescript plugin https://github.com/sitefinitysteve/nativescript-google-analytics
 */
declare module "nativescript-google-analytics" {

}
import * as googleAnalytics from "nativescript-google-analytics";
import * as application from "tns-core-modules/application";

export class TNSGoogleAnalytics {

    static init(trackingId: string) {
        googleAnalytics.initalize({
            trackingId: trackingId, //YOUR Id from GA
            dispatchInterval: 5,
            logging: {
                native: true,
                console: false
            }
        });
    }

    static logScreenView(screenName: string) {
        googleAnalytics.logView(screenName);
    }


    static logEvent(category: string, action: string, label?: string, value: number = 1) {
        googleAnalytics.logEvent(
            {
                category: category,
                action: action,
                label: label,
                value: value
            });
    }

    static startTimer(timerKey: string, category: string, name?: string) {
        googleAnalytics.startTimer(timerKey, {
            category: category,
            name: name, //Optional
            label: (application.ios) ? "iOS" : "Android"  //Optional
        });

        return function() {
            this.stopTimer = () => {
                googleAnalytics.stopTimer(timerKey);
            }
        }
    }


    static logTimingEvent(category: string, totalMilisecconds: number, name?: string, label?: string) {
        googleAnalytics.logTimingEvent({
            category: category,
            value: totalMilisecconds, //Milliseconds, example 4000
            name: name, //Optional
            label: label //Optional
        });
    }
}